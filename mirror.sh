#!/bin/bash
# GTFS mirroring tool.  This allows us to keep the GTFS data from Transport
# NSW into a revision control system (so we could look up historical data).
#
# Copyright 2011 Michael Farrell <http://micolous.id.au>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.
WGETRC="$HOME/transportnsw-auth"
DATA_ZIP="https://tdx.transportnsw.info/download/files/full_greater_sydney_gtfs_static.zip"
DATA_ZIPFILENAME="full_greater_sydney_gtfs_static.zip"
export WGETRC

# Create the target folder
mkdir -p "gtfs"

# Download data archive
# Normally -O would be used here, but it doesn't work in combination with -N.
# (see wget man page for details)
wget -N "${DATA_ZIP}"

# Shittypack the dataset
python shittypack/shittypack.py -o shittypack.zip "${DATA_ZIPFILENAME}"

# Extract the data into the data folder
unzip -oujL shittypack.zip -d gtfs

# Add files to commit
git add gtfs/*.txt

# Commit
DATE="`date`"
MSG="New data from upstream source on ${DATE}"

git commit -am "${MSG}"

# Push downstream to bitbucket
git push origin master
