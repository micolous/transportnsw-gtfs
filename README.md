# Transport for NSW GTFS mirror


This allows us to keep the GTFS data from Transport for NSW in a revision
control system (so we could look up historical data).

It should be trivial to adapt this to monitor another public transit
system.

This is best used in a daily crontab.

You shouldn't need to run this script yourself though, I already provide a
repository at <https://bitbucket.org/micolous/transportnsw-gtfs>.  It has data
starting from April 2015.


Copyright 2011,2014,2015 Michael Farrell <http://micolous.id.au>

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.

The data provided (in the `gtfs` folder) is licensed under the license
described in data_license.txt.

This repository uses the tool
[shittypack](https://github.com/micolous/shittypack) in order to
automatically reformat the contents of this dataset.  This generally
improves the quality of the source dataset by removing redundant, useless or
verbose data.  However, it is done without any human quality analysis, and
may be subject to additional errors not in the source data.

This work is based on [data from Transport for
NSW](https://tdx.transportnsw.info).
